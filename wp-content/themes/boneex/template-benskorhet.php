<?php
/*
Template Name: Benskörhet
*/
?>

<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="splash">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 aligncenter">
						<h1 class="entry-title"><?php the_field('custom_title'); ?></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 aligncenter">
						<a href="/#analys-formular" class="btn-lg btn-primary do-bone-analys-button">
							Gör ett <strong>benskörhetstest</strong> nu
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="about-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php wp_nav_menu( array( 'theme_location' => 'about-menu', 'container' => '') ); ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="content-container bread">
			<div class="container">
				<div class="row">
					<div class="col-xs-7">
						<h2><img src="<?php echo get_template_directory_uri(); ?>/images/heart-icon.png" alt="Ikon på ett hjärta">Fakta om benskörhet</h2>
						<span class="special">
							<p><?php the_field('fakta_om_benskorhet'); ?></p>
						</span>
					</div>
					<div class="col-xs-4 col-xs-offset-1">
						<h3>Symptom</h3>
						<?php if(get_field('symptom')) : ?>
						<ul class="list-dot">
							<?php while(has_sub_field('symptom')) : ?>
							<li><?php the_sub_field('name'); ?></li>
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>

						<h3>Riskfaktorer</h3>
						<?php if(get_field('riskfaktorer')) : ?>
						<ul class="list-dot">
							<?php while(has_sub_field('riskfaktorer')) : ?>
							<li><?php the_sub_field('name'); ?></li>
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>

						
						<img class="bone-disease-image" src="<?php echo get_template_directory_uri(); ?>/images/jawex.png" />
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
<?php get_footer(); ?>
