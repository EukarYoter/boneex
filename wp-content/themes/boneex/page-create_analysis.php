<?php header('Content-type: application/json'); ?>
<?php

function generateRandomString($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}
$errors = array();

if($_POST['answer_home'] == '1') {
	$answer = 'a:1:{i:0;s:10:"hemskickat";}';
} elseif($_POST['answer_mail'] == '1') {
	$answer = 'a:1:{i:0;s:4:"mail";}';
	$email = $_POST['email'];
	if($email == "") {
		$errors[] = 'Du måste ange en emailadress för att kunna få svar på din analys.';
	}
} else {
	$answer = 'a:1:{i:0;s:10:"hemskickat";}';
}


$villkor = $_POST['villkor'];
if($villkor == "0") {
	$errors[] = 'Du har inte markerat att du accepterar våra köpvillkor.';
}
if(sizeof($errors) > 0) {
	echo json_encode(array('success' => false, 'errors' => $errors));
} else {
	if( $_FILES["analys_image"]['name'] != '' ) {
		$upload_dir = wp_upload_dir();
		$filename = $upload_dir['path'].DIRECTORY_SEPARATOR.generateRandomString().'.jpg';	
		if(move_uploaded_file($_FILES["analys_image"]["tmp_name"], $filename)) {
			// Create post object
			$my_post = array(
				'post_title' => $_POST['fullname'],
				'post_status' => 'publish',
				'post_type' => 'boneex_bone_analysis',
				'post_author' => 1
			);
		 
			// Insert the post into the database
			$post_id = wp_insert_post( $my_post );

			$wp_filetype = wp_check_filetype(basename($filename), null );
			$wp_upload_dir = wp_upload_dir();
			$attachment = array(
				'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ), 
				'post_mime_type' => $wp_filetype['type'],
				'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
				'post_content' => '',
				'post_status' => 'publish'
			);
			$attach_id = wp_insert_attachment( $attachment, $filename, $post_id);
			// you must first include the image.php file
			// for the function wp_generate_attachment_metadata() to work
			require_once(ABSPATH . 'wp-admin/includes/image.php');
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			// Personnummer
			update_field( "field_526bcde0793c0", $_POST['ssn'], $post_id );

			// Personnummer
			update_field( "field_526bce09793c1", $_POST['fullname'], $post_id );

			// Personnummer
			update_field( "field_526bce25793c3", $_POST['adress'], $post_id );

			// Personnummer
			update_field( "field_526bce34793c4", $_POST['zip'], $post_id );

			// Personnummer
			update_field( "field_526bce3e793c5", $_POST['ort'], $post_id );

			// Mail
			update_field( "field_526bce49793c6", $_POST['email'], $post_id );

			// Svar via...
			update_field( "field_526bce5a793c7", $answer, $post_id );

			// Personnummer
			update_field( "field_526bce98793c8", $attach_id, $post_id );
			

			/* ORDERBEKRÄFTELSE */
			$ssn = crypt_ssn($_POST['ssn']);
			$fullname = $_POST['fullname'];
			$adress = $_POST['adress'];
			$ort = $_POST['ort'];
			$zip = $_POST['zip'];
			$email = $_POST['email'];

			$to = $_POST['email'];
			$subject = "Boneprox orderbekräftelse";
			if($_POST['answer_home'] == '1') {
			$message = "
			<h3>Tack för att du har beställt en analys av oss på Boneprox! </h3>
			<br />
			Vi har mottagit din information och jobbar för att du ska få svar inom två arbetsdagar. <br />
			<br />			 
			Du har valt att få ditt testresultat hemskickat till $adress, $zip $ort
			<br />			 
			-------------------------------------------------------------------------------------------------------------------------------<br />
			<br />
			<br />			 
			Dina uppgifter: <br />
			$ssn<br />
			$fullname<br />
			$adress<br />
			$ort<br />
			$zip<br />
			$email<br />
			 <br />
			-------------------------------------------------------------------------------------------------------------------------------<br />
			 <br />
			Pris<br />
			Analys 399 sek<br />
			Svar via mail 0 sek<br />
			 <br />
		 	Totalt 418 sek<br />
			Varav moms 83,6 sek<br />
			 <br />
			Fakturan kommer att skickas till ovanstående uppgifter.<br />
			Är det någonting som inte stämmer eller om du har några frågor är det bara att höra av dig till oss!<br />
			 <br />
			-------------------------------------------------------------------------------------------------------------------------------<br />
			 <br />
			 <br />
			BONEPROX AB<br />
			Org.nr 556945-1882<br />
			www.Boneprox.se<br />
			Långgatan 29<br />
			571 33, Nässjö<br />
			F-skatt här<br />
			Momsreg här<br />
			 <br />
			Mail: Info@Boneprox.se<br />
			Telefon: 0702-80 55 99<br />
			";
		} else if($_POST['answer_mail'] == '1') {
			$message = "
			<h3>Tack för att du har beställt en analys av oss på Boneprox! </h3>
			<br />
			Vi har mottagit din information och jobbar för att du ska få svar inom två arbetsdagar. <br />
			 <br />
			Du har valt att få ditt testresultat skickat till din mail: $email<br />
			 <br />
			-------------------------------------------------------------------------------------------------------------------------------<br />
			 <br />
			Dina uppgifter: <br />
			$ssn<br />
			$fullname<br />
			$adress<br />
			$ort<br />
			$zip<br />
			$email<br />
			 <br />
			-------------------------------------------------------------------------------------------------------------------------------<br />
			 <br />
			Pris<br />
			Analys 399 sek<br />
			Svar via mail 0 sek<br />
			 <br />
			Totalt 399 sek<br />
			Varav moms 79,8 sek<br />
			 <br />
			Fakturan kommer att skickas till ovanstående uppgifter.<br />
			Är det någonting som inte stämmer eller om du har några frågor är det bara att höra av dig till oss!<br />
			 <br />
			-------------------------------------------------------------------------------------------------------------------------------<br />
			 <br />
			 <br />
			BONEPROX AB<br />
			Org.nr 556945-1882<br />
			www.Boneprox.se<br />
			Långgatan 29<br />
			571 33, Nässjö<br />
			F-skatt här<br />
			Momsreg här<br />
			 <br />
			Mail: Info@Boneprox.se<br />
			Telefon: 0702-80 55 99<br />
			";
		}
			wp_mail( $to, $subject, $message);

			echo json_encode(array('success' => true));
		} else {
			$errors = array();
			$errors[] = 'Ett oväntat fel inträffad med bilduppladdningen, vänligen försök igen!';
			echo json_encode(array('success' => false, 'errors' => $errors));
		}
	}
	else {
		$errors = array();
		$errors[] = 'Du har inte valt någon bild';
		echo json_encode(array('success' => false, 'errors' => $errors));
	}
}