<?php
/*
Template Name: JawEx Metoden
*/
?>

<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="splash">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 aligncenter">
						<h1 class="entry-title"><?php the_field('custom_title'); ?></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 aligncenter">
						<a href="/#analys-formular" class="btn-lg btn-primary do-bone-analys-button">
							Gör ett <strong>benskörhetstest</strong> nu
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="about-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php wp_nav_menu( array( 'theme_location' => 'about-menu', 'container' => '') ); ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="content-container bread">
			<div class="container">
				<div class="row">
					<div class="col-xs-7">
						<h2><img src="<?php echo get_template_directory_uri(); ?>/images/heart-icon.png" alt="Ikon på ett hjärta">Vad är Jaw-X?</h2>
						<?php the_field('vad_ar_jawex'); ?>
						<h2><img src="<?php echo get_template_directory_uri(); ?>/images/mark-icon.png" alt="Ikon på en accepterad bock">Hur fungerar det?</h2>
						<?php the_field('hur_fungerar_det'); ?>
					</div>
					<div class="col-xs-4 col-xs-offset-1">
						<img src="<?php echo get_template_directory_uri(); ?>/images/bone_disease.png" />
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
<?php get_footer(); ?>
