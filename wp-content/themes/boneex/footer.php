	</div> <!-- content -->
	<?php if(!is_front_page()) : ?>
	<div class="footer-analysis">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 aligncenter">
					<p><img src="<?php echo get_template_directory_uri(); ?>/images/people-icon.png" alt="Människor illustration">Vi har redan analyserat <span class="featured"><?php echo get_analysis_count(); ?></span> personers <strong>tandröntgenbilder</strong><a class="btn-sm btn-primary" href="<?php echo home_url(); ?>/#analys-formular">Testa dig nu</a></p>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-3">
					<h3><?php echo get_option('theme_company'); ?></h3>
					<p>
						<strong>Telefon</strong> <?php echo get_option('theme_phone'); ?><br />
						<strong>Mail</strong> <a href="mailto:<?php echo get_option('theme_mail'); ?>"><?php echo get_option('theme_mail'); ?></a><br />
						<strong>Org. Nr.</strong> <?php echo get_option('theme_org_nr'); ?><br />
						<a class="bold" href="/om-boneprox/#wpcf7-f133-p17-o1">Kontakta oss</a>
					</p>
				</div>
				<div class="col-xs-3">
					<h3>Information</h3>
					<div class="footer-menu">
						<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => '') ); ?>
					</div>
				</div>
				<div class="col-xs-6">
					<h3>Våra samarbetspartners</h3>
					<?php $partners = get_partners(); ?>
					<div class="row">
					<?php foreach($partners as $partner): ?>
						<div class="col-xs-3">
							<a href="<?php the_field('lank', $partner->id); ?>">
								<img src="<?php the_field('logotype', $partner->id); ?>" alt="<?php the_field('foretag', $partner->id); ?>">
							</a>
						</div>
					<?php endforeach; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6 footer-copyright">
					Copyright &copy; 2013, Boneprox.se
				</div>
			</div>
		</div>
	</div>
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/app.js"></script>
	<?php wp_footer(); ?>
</body>
</html>