<?php

include('inc/architecture.php');
//include('inc/acf.php');
include('inc/settings.php');
include('inc/acf-repeater/acf-repeater.php');

function register_my_menus() {
  register_nav_menus(
    array(
      'primary-menu' => __( 'Primär meny' ),
      'footer-menu' => __( 'Footer meny' ),
      'about-menu' => __( 'Extra meny' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

function my_filter_head() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'my_filter_head');

add_filter('show_admin_bar', '__return_false');

function custom_wp_mail_from( $original_email_address )
{
  return 'noreply@boneprox.se';
}
add_filter( 'wp_mail_from', 'custom_wp_mail_from' );

function custom_wp_mail_from_name( $original_email_from )
{
  return 'Boneprox AB';
}
add_filter( 'wp_mail_from_name', 'custom_wp_mail_from_name' );

function custom_wp_mail_content_type() {
  return "text/html";
}
add_filter('wp_mail_content_type', 'custom_wp_mail_content_type');

function get_analysis_count() {
	global $wpdb;

	$wpdb->get_results("SELECT id FROM $wpdb->posts WHERE post_type = 'boneex_bone_analysis' AND post_status = 'publish'" );
	return (int)$wpdb->num_rows + 4018;
}

function crypt_ssn($ssn) {
  switch(strlen($ssn)) {
    case 10: 
      $ssn_year = substr($ssn, 0, 2);
      $ssn_visible_numbers = substr($ssn, 8, 2);
      $crypted_ssn = $ssn_year . 'XX' . 'XX' . '-' . 'XX' . $ssn_visible_numbers;
      break;

    case 11: 
      $ssn_year = substr($ssn, 0, 2);
      $ssn_visible_numbers = substr($ssn, 9, 2);
      $crypted_ssn = $ssn_year . 'XX' . 'XX' . '-' . 'XX' . $ssn_visible_numbers;
      break;

    case 12: 
      $ssn_year = substr($ssn, 0, 4);
      $ssn_visible_numbers = substr($ssn, 10, 2);
      $crypted_ssn = $ssn_year . 'XX' . 'XX' . '-' . 'XX' . $ssn_visible_numbers;
      break;

    case 13: 
      $ssn_year = substr($ssn, 0, 4);
      $ssn_visible_numbers = substr($ssn, 11, 2);
      $crypted_ssn = $ssn_year . 'XX' . 'XX' . '-' . 'XX' . $ssn_visible_numbers;
      break;

    default: 
      $ssn_year = substr($ssn, 0, 4);
      $ssn_visible_numbers = substr($ssn, 11, 2);
      $crypted_ssn = $ssn_year . 'XX' . 'XX' . '-' . 'XX' . $ssn_visible_numbers;
      break;
  }

  return $crypted_ssn;
}

function get_partners() {
  global $wpdb;
  return $wpdb->get_results("SELECT id FROM $wpdb->posts WHERE post_type = 'boneex_partners' AND post_status = 'publish'" );
}
