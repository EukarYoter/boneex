<?php
/*
Template Name: Om Boneex
*/
?>

<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="splash">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 aligncenter">
						<h1><?php the_field('custom_title'); ?></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 aligncenter">
						<a href="/#analys-formular" class="btn-lg btn-primary do-bone-analys-button">
							Gör ett <strong>benskörhetstest</strong> nu
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="about-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php wp_nav_menu( array( 'theme_location' => 'about-menu', 'container' => '') ); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="content-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-7">
						<div class="entry-content">
							<h2>Om företaget Boneprox</h2>
							<span class="bread">
							<?php the_field('om_foretaget'); ?>
							<h2>Vår vision</h2>
							<?php the_field('var_vision'); ?>
							</span>
							<h2>Kontakta oss</h2>
							<?php echo do_shortcode('[contact-form-7 id="133" title="Kontaktformulär"]'); ?>
						</div>
					</div>
					<div class="col-xs-4 col-xs-offset-1">
						<h2><img src="<?php echo get_template_directory_uri(); ?>/images/mail-icon.png" alt="Mail ikon">Kontaktuppgifter</h2>
						<p>
							<strong>Telefon:</strong> <?php echo get_option('theme_phone'); ?><br />
							<strong>Mail</strong> <a href="mailto:<?php echo get_option('theme_mail'); ?>"><?php echo get_option('theme_mail'); ?></a>
						</p>
						<h2><img src="<?php echo get_template_directory_uri(); ?>/images/business-icon.png" alt="Mail ikon">Företagsuppgifter</h2>
						<p>
							<strong><?php echo get_option('theme_commpany'); ?></strong> <br />
							Org. Nr. <?php echo get_option('theme_org_nr'); ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
<?php get_footer(); ?>