boneex = {};

boneex.readMoreButtons = {
	init:function() {
		$('.short-text a[href="#"]').on('click', function(e) {
			var that = this;
			$(this).parent().slideUp(100, function() {
				$(that).parent().next().slideDown(100);
			});

			return false;
		});
	}
}

boneex.analysForm = {
	init: function() {
		boneex.analysForm.calcPrice();
		boneex.analysForm.toggleTerm();
		boneex.analysForm.upload();

		$('.answer-home, .answer-mail').on('click', function() {
			boneex.analysForm.changeDelivery($(this).attr('class'));
		});

		$('#test-form input[type="file"]').on('change', function() {
			if($(this).val() != '') {
				$('.file-upload').html('<p>Bilden är nu uppladdad</p>');
			}
		});

		$('#test-form').on('submit', function() {
			boneex.analysForm.submitForm($(this));
			return false;
		});
	},
	clearForm: function() {
		$('input[name="ssn"]').val('');
		$('input[name="fullname"]').val('');
		$('input[name="adress"]').val('');
		$('input[name="ort"]').val('');
		$('input[name="zip"]').val('');
		$('input[name="email"]').val('');
		$('input[name="villkor"]').val(0);
	},
	changeDelivery: function(delivery_type) {
		switch(delivery_type) {
			case 'answer-mail':
				$('.answer-home').find('.box').removeClass('checked');
				$('.answer-mail').find('.box').addClass('checked');
				$('input[name="answer_home"]').val(0);
				$('input[name="answer_mail"]').val(1);
				$('.delivery-price').html('0');
				break;
			case 'answer-home':
				$('.answer-home').find('.box').addClass('checked');
				$('.answer-mail').find('.box').removeClass('checked');
				$('input[name="answer_home"]').val(1);
				$('input[name="answer_mail"]').val(0);
				$('.delivery-price').html('19');
				break;
		}
		boneex.analysForm.calcPrice();
	},
	upload: function() {
		$('.file-upload').on('click', function() {
			$('#test-form input[type="file"]').trigger('click');
		});
	},
	toggleTerm: function() {
		$('.villkoren').on('click', function() {
			$(this).find('.box').toggleClass('checked');
			if($(this).find('.box').hasClass('checked')) {
				$('input[name="villkor"]').val(1);
			} else {
				$('input[name="villkor"]').val(0);
			}
		});

		$('.terms-toggle').on('click', function() {
			$('#terms-modal').modal('toggle');
		});
	},
	calcPrice: function() {
		$('.price-sum').html(parseInt($('.analysis-price').html()) + parseInt($('.delivery-price').html()));
	},
	submitForm: function(form, e) {
		var formdata = new FormData(document.forms.namedItem('test-form'));
		
		$.ajax({
			url: '/api/post/create_analysis/',
			type: 'post',
			data: formdata,
	        processData: false,
            contentType: false,
            datatype: 'json',
            beforeSend: function() {
            	$('#test-form input[type="submit"]').hide();
            	$('#test-form-message').addClass('alert alert-info');
            	$('<p>Laddar...</p>').appendTo('#test-form-message');
            	$('#test-form-message').show(200);
            },
	        success: function(data) {
	        	$('#test-form input[type="submit"]').show();
	        	if(data.success) {
	        		$('#test-form-message').empty();
        			$('#test-form-message').removeClass('alert alert-danger alert-info');
        			$('#test-form-message').addClass('alert alert-success');
        			$('#test-form-message').show(200);
        			$('<p>Din analys blev skapad och vi hör av oss så snart den är klar!</p>').appendTo('#test-form-message');
        			boneex.analysForm.clearForm();
	        	} else {
	        		$('#test-form-message').empty();
	        		$.each(data.errors, function(i, value) {
	        			$('#test-form-message').removeClass('alert alert-success alert-info');
	        			$('#test-form-message').addClass('alert alert-danger');
	        			$('#test-form-message').show(200);
	        			$('<p>'+value+'</p>').appendTo('#test-form-message');
	        		});
	        	}
	        },
	        complete: function() {
	        	$('html, body').animate({
			        scrollTop: $("#test-form-message").offset().top - 80
			    }, 500);
	        }
		})
	}
}
boneex.contactLinkButtons = {
	init: function() {

		var jump=function(e) {
	       	var target = location.hash;

			if (e){

				if(location.pathname != $(this).attr('href')) {
					location.replace($(this).attr('href'));
				}

				e.preventDefault();
				var target = $(this).attr("href")
					.replace('/', '')
					.replace('om-boneex/', '');
			} else{
				var target = location.hash;
			}

			if(target == "#analys-formular") {
					var scroll_to = $(target).offset().top - 194;
			}

			if(target == "#wpcf7-f133-p17-o1") {
					var scroll_to = $(target).offset().top - 100;
			}

			$('html,body').animate({
			   scrollTop: scroll_to
			},1000, function() {
					location.hash = target;
			});
		}

		$('a[href="/#analys-formular"]').bind("click", jump);
		$('a[href="#analys-formular"]').bind("click", jump);
		$('a[href="/om-boneex/#wpcf7-f133-p17-o1"]').bind("click", jump);

		if (location.hash){
			setTimeout(function(){
				$('html, body').scrollTop(0).show();
				jump();
			}, 0);
		} else{
			$('html, body').show();
		}
	}
}
$(function() {
	boneex.readMoreButtons.init();
	boneex.analysForm.init();
	boneex.contactLinkButtons.init();
});