<?php
/*
Template Name: Privacy Policy
*/
?>

<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="splash">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 aligncenter">
						<h1 class="entry-title"><?php the_field('custom_title'); ?></h1>
					</div>
				</div>
				<div class="col-xs-12 aligncenter">
					<a href="/#analys-formular" class="btn-lg btn-primary do-bone-analys-button">
						Gör ett <strong>benskörhetstest</strong> nu
					</a>
				</div>
			</div>
		</div>

		<div class="about-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php wp_nav_menu( array( 'theme_location' => 'about-menu', 'container' => '') ); ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="content-container bread">
			<div class="container">
				<div class="row">
					<div class="col-xs-7">
						<h2>Privacy Policy</h2>
						<p class="bread">
							<?php the_field('privacy_policy'); ?>
						</p>
						<h2>Cookies</h2>
						<p class="bread">
							<?php the_field('cookies'); ?>
						</p>
					</div>
					<div class="col-xs-4 col-xs-offset-1">
						<?php if(get_field('sidebar_box')) : ?>
							<?php while(has_sub_field('sidebar_box')) : ?>
								<h3><img src="<?php the_sub_field('ikon'); ?>" /><?php the_sub_field('titel'); ?></h3>
								<p><?php the_sub_field('text'); ?></p>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
<?php get_footer(); ?>
