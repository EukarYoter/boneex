<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="sv">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="sv">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="sv">
<!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<!--[if lt IE 9]>
      <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>">

  <link href="<?php echo get_template_directory_uri(); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon">
	<?php wp_head(); ?>
</head>

<body>
	<div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
          	<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Boneprox Logotype">
          	<span>Boneprox</span>
          </a>
        </div>
        <div class="collapse navbar-collapse pull-right">
          <?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'menu_class' => 'nav navbar-nav') ); ?>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    <div class="content">
