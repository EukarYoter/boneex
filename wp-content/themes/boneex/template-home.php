<?php
/*
Template Name: Startsida
*/
?>

<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<div class="splash">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 aligncenter">
					<h1 class="entry-title"><?php the_field('custom_title'); ?></h1>
					<h2 class="entry-sub-title"><?php the_field('custom_sub_title'); ?></h2>
				</div>
			</div>
			<div class="col-xs-12 aligncenter">
					<a href="/#analys-formular" class="btn-lg btn-primary do-bone-analys-button-start">
						Gör ett <strong>benskörhetstest</strong> nu
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="start-boxes">
		<div class="container">
			<div class="row">
				<?php if(get_field('start_boxes')) : ?>
					<?php $counter = 1; ?>
					<?php while(has_sub_field('start_boxes')) : ?>
						<?php if($counter ==  1) : ?>
							<div class="col-xs-3">
						<?php elseif($counter ==  2) : ?>
							<div class="col-xs-5">
						<?php elseif($counter ==  3) : ?>
							<div class="col-xs-4">
						<?php endif;?>
							<img src="<?php the_sub_field('ikon'); ?>" alt="Ikon">
							<p><?php the_sub_field('text'); ?></p>
						</div>
						<?php $counter++; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="content-container bread">
		<div class="container">
			<div class="row">
				<div class="col-xs-6 smaller-6">
					<h2><img src="<?php echo get_template_directory_uri(); ?>/images/heart-icon.png" alt="Ikon på ett hjärta">Varför ett benskörhetstest?</h2>
					<p><?php the_field('why_do_a_test'); ?></p>
					<a id="test-form-link" class="btn-sm btn-primary pull-left" href="<?php echo home_url(); ?>/benskorhet/">Läs mer om benskörhet</a>
				</div>
				<div class="col-xs-6 pull-right">
					<h2><img src="<?php echo get_template_directory_uri(); ?>/images/mark-icon.png" alt="Ikon på en accepterad bock">Ett effektivt sätt att upptäcka benskörhet</h2>
					<p>
						<img src="/wp-content/uploads/2013/10/tooth-doctor.png" style="float: right"><?php the_field('effektivt_satt'); ?>
					</p>
					<a class="btn-start btn-sm btn-primary pull-right" href="<?php echo home_url(); ?>/jawex-metoden/">Läs mer om metoden</a>
				</div>
			</div>
		</div>
	</div>

	<div class="start-analysis">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 aligncenter">
					<p><img src="<?php echo get_template_directory_uri(); ?>/images/people-icon.png" alt="Människor illustration">Vi har redan analyserat <span class="featured"><?php echo get_analysis_count(); ?></span> personers <strong>tandröntgentbilder</strong> - låt oss analysera din med!</span></p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="content-container" id="analys-formular">
		<div class="container">
			<div class="row">
				<div class="col-xs-6">
					<h2 class="h1 featured aligncenter">Testa dig nu</h2>
					<form id="test-form" name="test-form" method="post" enctype="multipart/form-data">
						<fieldset>
							<h2><span>1</span>Fyll i dina uppgifter</h2>
							<input type="text" name="ssn" placeholder="Personnummer *" required>
							<input type="text" name="fullname" placeholder="Fullständigt namn *" required>
							<input type="text" name="adress" placeholder="Adress *" required>
							<input type="text" name="ort" placeholder="Ort *" required>
							<input type="text" name="zip" placeholder="Postnummer *" required>
							<input type="email" name="email" placeholder="Mailadress *" required>
							<input type="hidden" name="answer_home" value="1">
							<input type="hidden" name="answer_mail" value="0">
							<div class="checkboxes">
								<div class="answer-home"><span class="box"></span>Jag vill få svar <strong>hemskickat</strong></div>
								<div class="answer-mail"><span class="box checked"></span>Jag vill få svar <strong>via mail</strong></div>
							</div>
						</fieldset>
						<fieldset>
							<h2><span>2</span>Ladda upp din röntgenbild</h2>
							<div class="file-upload">
								<div class="row">
									<div class="col-xs-8">
										<p>Klicka här för att välja röntgenbild</p>
									</div>
									<div class="col-xs-4">
										<a class="btn btn-primary">Ladda upp</a>
									</div>
								</div>
							</div>
							<input type="file" name="analys_image" id="analys_image">
							<p class="small">OBS! Tydlig bild på underkäken (Premolar stående biting), <a target="_blank" class="bold" href="/hur">Läs mer här</a></p>
						</fieldset>
						<div id="test-form-message" class="test-form-message aligncenter"></div>
						<input type="hidden" name="villkor" value="0">
						<div class="checkboxes">
							<div class="villkoren"><span class="box"></span>Jag accepterar <a class="terms-toggle">villkoren</a></div>
						</div>
						<input type="submit" class="btn btn-primary" value="Genomför analys">
					</form>
				</div>
				<div class="col-xs-4 col-md-offset-1">
					<h2 class="aligncenter regular">Såhär fungerar det</h2>
					<?php if(get_field('sidebar_box')) : ?>
						<?php while(has_sub_field('sidebar_box')) : ?>
							<h3><img src="<?php the_sub_field('ikon'); ?>" alt=""><?php the_sub_field('titel'); ?></h3>
							<span class="bread">
								<p class="short-text"><?php the_sub_field('kort_text'); ?> <a href="#">Läs mer</a></p>
								<p class="long-text"><?php the_sub_field('lang_text'); ?></p>
							</span>
						<?php endwhile; ?>
					<?php endif; ?>
					<h2 class="aligncenter regular-no-margin">Prisinformation</h2>
					<div class="price-information">
						<div class="row">
							<div class="col-xs-1"></div>
							<div class="col-xs-7">Benskörhetsanalys</div>
							<div class="col-xs-3 alignright featured"><span class="analysis-price"><?php echo get_option('theme_price'); ?></span> sek</div>
						</div>
						<div class="row choosen-delivery-method">
							<div class="col-xs-1"></div>
							<div class="col-xs-7">Fraktsätt</div>
							<div class="col-xs-3 alignright featured"><span class="delivery-price">0</span> sek</div>
						</div>
						<div class="row price-total">
							<div class="col-xs-1"></div>
							<div class="col-xs-6 price-text"><strong>Totalt</strong></div>
							<div class="col-xs-4 alignright featured"><span class="price-sum"></span> sek</div>
						</div>
						<p class="small-price-text">Betala först när du fått ditt testresultat. Läs mer om våra <a class="terms-toggle">köpvillkor här</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endwhile; ?>
	<div class="modal fade" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h2 class="modal-title" id="myModalLabel">Våra köpvillkor</h2>
				</div>
				<div class="modal-body">
					<h2>Betalningsalternativ</h2>
						<?php the_field('betalningsalternativ', 15); ?>
						<h2>Skydd av personuppgifter och anonymitet</h2>
						<?php the_field('pul', 15); ?>
						<h2>Pris och svarstider</h2>
						<?php the_field('pris_och_svarstider', 15); ?>
						<?php if(get_field('leveranssätt', 15)) : ?>
						<?php while(has_sub_field('leveranssätt', 15)) : ?>
							<h3><?php echo get_sub_field('titel', 15); ?> <span class="main-color"><?php echo get_sub_field('pris', 15); ?> kr</span></h3>
							<p><?php echo get_sub_field('text', 15); ?></p>
						<?php endwhile; ?>
						<?php endif; ?>

						<h2>Tjänstens begränsningar</h2>
						<?php the_field('begransningar', 15); ?>

						<h2>Ansvar</h2>
						<?php the_field('ansvar', 15); ?>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<?php get_footer(); ?>
