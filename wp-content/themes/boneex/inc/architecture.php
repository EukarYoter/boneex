<?php

// Register Custom Post Type
function create_bone_analysis_post_type() {

	$labels = array(
		'name'                => _x( 'Analyser', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Analys', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Analyser', 'text_domain' ),
		'parent_item_colon'   => __( '', 'text_domain' ),
		'all_items'           => __( 'Alla analyser', 'text_domain' ),
		'view_item'           => __( 'Visa analys', 'text_domain' ),
		'add_new_item'        => __( 'Skapa ny analys', 'text_domain' ),
		'add_new'             => __( 'Ny analys', 'text_domain' ),
		'edit_item'           => __( 'Redigera analys', 'text_domain' ),
		'update_item'         => __( 'Uppdatera analys', 'text_domain' ),
		'search_items'        => __( 'Sök Analyser', 'text_domain' ),
		'not_found'           => __( 'Inga analyser', 'text_domain' ),
		'not_found_in_trash'  => __( 'Inga analyser i papperskorgen', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'boneex_bone_analysis', 'text_domain' ),
		'description'         => __( 'Benskörhetsanalyser', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'boneex_bone_analysis', $args );

}

// Hook into the 'init' action
add_action( 'init', 'create_bone_analysis_post_type', 0 );

// Register Custom Post Type
function create_partner_post_type() {

	$labels = array(
		'name'                => _x( 'Samarbetspartners', 'Post Type General Name', 'create_partner_post_type' ),
		'singular_name'       => _x( 'Samarbetspartner', 'Post Type Singular Name', 'create_partner_post_type' ),
		'menu_name'           => __( 'Partners', 'create_partner_post_type' ),
		'parent_item_colon'   => __( '', 'create_partner_post_type' ),
		'all_items'           => __( 'Alla partners', 'create_partner_post_type' ),
		'view_item'           => __( 'Visa partner', 'create_partner_post_type' ),
		'add_new_item'        => __( 'Skapa ny samarbetspartner', 'create_partner_post_type' ),
		'add_new'             => __( 'Ny partner', 'create_partner_post_type' ),
		'edit_item'           => __( 'Redigera partner', 'create_partner_post_type' ),
		'update_item'         => __( 'Uppdatera partner', 'create_partner_post_type' ),
		'search_items'        => __( 'Sök bland partners', 'create_partner_post_type' ),
		'not_found'           => __( 'Inga partners', 'create_partner_post_type' ),
		'not_found_in_trash'  => __( 'Inga partners i soptunnan', 'create_partner_post_type' ),
	);
	$args = array(
		'label'               => __( 'boneex_partners', 'create_partner_post_type' ),
		'description'         => __( 'Samarbetspartners', 'create_partner_post_type' ),
		'labels'              => $labels,
		'supports'            => array( 'title'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => false,
		'menu_position'       => 20,
		'menu_icon'           => '',
		'can_export'          => false,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'boneex_partners', $args );

}

// Hook into the 'init' action
add_action( 'init', 'create_partner_post_type', 0 );