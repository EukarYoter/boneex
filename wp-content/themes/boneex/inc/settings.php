<?php
add_action("admin_menu", "setup_theme_settings");
function setup_theme_settings() {  
    add_submenu_page('themes.php',   
        'Theme settings', 'Temainställningar', 'manage_options',   
        'theme-settings', 'theme_settings');   
}
function theme_settings() {
    
    if (!current_user_can('manage_options')) {  
        wp_die('You do not have sufficient permissions to access this page.');  
    }
    if (isset($_POST["update_settings"])) {  
    	$company = $_POST["company"];
        $org_nr = $_POST["org_nr"];
        $phone = $_POST["phone"];
        $mail = $_POST["mail"];
        $price = $_POST["price"];
        update_option("theme_company", $company);
        update_option("theme_org_nr", $org_nr);
        update_option("theme_phone", $phone);
        update_option("theme_mail", $mail);
        update_option("theme_price", $price);
    }
    
    $theme = wp_get_theme();
    
?>  
    <div class="wrap">  
        <?php screen_icon('themes'); ?> <h2>Temainställningar</h2>  
        <h3>Tema: <?php echo $theme->Name; ?> v<?php echo $theme->Version; ?></h3>
        <form method="POST" action="">
        	<h4>Generella inställningar</h4>
			<table class="form-table">
                <tr valign="top">  
                    <th scope="row">  
                        <label for="company">  
                            Företagsnamn
                        </label>   
                    </th>  
                    <td>  
                        <input 
                            type="text" 
                            id="company" 
                            name="company"
                            size="25" 
                            value="<?php echo get_option("theme_company"); ?>" 
                        />  
                    </td>  
                </tr>
				<tr valign="top">  
                    <th scope="row">  
                        <label for="org_nr">  
                            Organisationsnummer
                        </label>   
                    </th>  
                    <td>  
                        <input 
                        	type="text" 
                        	id="org_nr" 
                        	name="org_nr"
                        	size="25" 
                        	value="<?php echo get_option("theme_org_nr"); ?>" 
                    	/>  
                    </td>  
                </tr>
            </table>
            <h4>Prisuppgifter</h4>
            <table class="form-table">
                <tr valign="top">  
                    <th scope="row">  
                        <label for="price">  
                            Pris för en Analys
                        </label>   
                    </th>  
                    <td>  
                        <input 
                            type="number" 
                            id="price" 
                            name="price"
                            size="25" 
                            value="<?php echo get_option("theme_price"); ?>" 
                        />  
                    </td>  
                </tr>
            </table>
            <h4>Kontakt</h4>
            <table class="form-table">
                <tr valign="top">  
                    <th scope="row">  
                        <label for="phone">  
                            Telefonnummer
                        </label>   
                    </th>  
                    <td>  
                        <input 
                            type="text" 
                            id="phone" 
                            name="phone"
                            size="25" 
                            value="<?php echo get_option("theme_phone"); ?>" 
                        />  
                    </td>  
                </tr>
                <tr valign="top">  
                    <th scope="row">  
                        <label for="mail">  
                            Mail
                        </label>   
                    </th>  
                    <td>  
                        <input 
                            type="text" 
                            id="mail" 
                            name="mail"
                            size="25" 
                            value="<?php echo get_option("theme_mail"); ?>" 
                        />  
                    </td>  
                </tr>
            </table> 
        <input type="hidden" name="update_settings" value="Y" />
        
        <p>  
        <input type="submit" value="Save settings" class="button-primary"/>  
        </p>
        
        </form>  
    </div>
<?php } 